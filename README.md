# Gaussian Elimination

[![pipeline status](https://gitlab.com/Marc--Olivier/gaussian-elimination/badges/master/pipeline.svg)](https://gitlab.com/Marc--Olivier/gaussian-elimination/commits/master)
&emsp;
[![coverage report](https://gitlab.com/Marc--Olivier/gaussian-elimination/badges/master/coverage.svg)](https://gitlab.com/Marc--Olivier/gaussian-elimination/commits/master)

## Description

This repository provides an implementation of the
[Gauss-Jordan algorithm](https://www.csun.edu/~panferov/math262/262_rref.pdf)
for matrices that contain
[FieldElement](http://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/FieldElement.html)
objects such as
[Fraction](http://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/fraction/Fraction.html)
or
[BigFraction](http://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/fraction/BigFraction.html).

The advantage of using `BigFraction` over `Fraction` is that `BigFraction` does
not throw overflow exceptions. It seems that matrices based on `Fraction` have
difficulties to handle (relatively) random matrices of size `14 x 14` as seen in
[GaussianEliminationBenchmark](src/jmh/java/org/andrez/algebra/linear/GaussianEliminationBenchmark.java#L14).

However, the performances when using `BigFraction` are not as good as when using
`Fraction`. To see the difference, look at the results from the CI job
`micro-benchmark`, or run the following command:

```sh
gradle --no-daemon jmh
```

_See also_

- [FieldMatrix](src/main/java/org/andrez/algebra/linear/internal/FieldMatrix.java)
  for a representation of matrices that contain
  [FieldElement](http://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/FieldElement.html).
- [GaussianElimination](src/main/java/org/andrez/algebra/linear/GaussianElimination.java)
  for an implementation of the Gauss-Jordam algorithm

## TODOs

At the moment, the goal of this repository is to test different data structures
for the representation of a matrix that contains rational numbers, and
algorithms to perform the Gauss-Jordan algorithm on such matrices.

Below is a list of things that could be done:

- [ ] Add unit tests for `FieldMatrix<BigFraction>`, especially for the method
      `toString`.

- [ ] Explore more data structures to represent the matrix in a way that
      generates less objects:

  - `Matrix<Fraction>` of size `n x m` contains `n x m` `Fraction` objects, each
    containing two `int`. Instead, the matrix could store an array of `long` and
    provides methods to retrieve the numerator and denominator for each element
    `(i,j)`.
  - `Matrix<BigFraction>` of size `n x m` contains `n x m` `BigFraction`
    objects, each containing two `BigInteger`. The two BigInteger could possibly
    be merged into one `BigInteger`.

- [ ] Add a Command Line Interface (CLI) that parses an input text which
      describes a matrix, and returns the reduced row echelon form of this
      matrix in the same text format.

  - The text representation of the matrix could be similar as the one used on
    https://people.revoledu.com/kardi/tutorial/LinearAlgebra/RREF.html.

- [ ] Add CI jobs to create and publish JARs.

- [ ] Improve the CI system for reusing produced or fetched artifacts between CI
      jobs.

## CI tools

### Docker image with JDK 11.0.3 and Gradle 5.4.1

The CI system is based on GitLab (see [.gitlab-ci.yml](.gitlab-ci.yml)), which
uses a Docker image to run the different CI jobs.

The default Docker image
[mandrez/jdk-11-gradle](https://cloud.docker.com/u/mandrez/repository/docker/mandrez/jdk-11-gradle)
is defined in [Dockerfile](ci/docker-image-openjdk-11/Dockerfile).

To generate a new version of the image, use the script
[create-image](ci/docker-image-openjdk-11/create-image):

```sh
$ ./ci/docker-image-openjdk-11/create-image
...
Successfully tagged mandrez/jdk-11-gradle:20190602133323
```

When it is finished, push the image to the Docker repository:

```sh
$ docker push mandrez/jdk-11-gradle:20190602133323
```

### Others

#### Formatting markdown files

I use [prettier](https://prettier.io) for formatting the markdown files such as
README.md:

```sh
prettier --prose-wrap=always --write **/*.md
```

To install prettier, you can use [npm](https://www.npmjs.com):

```sh
npm install -g prettier
```
