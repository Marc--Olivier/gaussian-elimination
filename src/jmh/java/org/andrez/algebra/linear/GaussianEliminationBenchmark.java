package org.andrez.algebra.linear;

import static org.andrez.algebra.linear.FractionUtils.r;

import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.fraction.Fraction;
import org.openjdk.jmh.annotations.*;

public class GaussianEliminationBenchmark {

  @State(Scope.Benchmark)
  public static class MyState extends RandomFractionVectorFactory<Fraction> {

    // reducedRowEchelonForm throws an overflow exception for a matrix of size 15.
    @Param({"4", "8", "14"})
    private int size;

    @Setup(Level.Trial)
    public void setup() {
      randomVector = createRandomVector(size * size);
    }

    @Override
    Fraction createElement(int numerator, int denominator) {
      return r(numerator, denominator);
    }

    @Override
    Fraction[] createVector(int size) {
      return new Fraction[size];
    }

    @Override
    int getSize() {
      return size;
    }
  }

  @Benchmark
  @BenchmarkMode(Mode.Throughput)
  public void reducedRowEchelonForm(MyState state) {
    var gaussianElimination = state.createGaussianElimination();
    try {
      gaussianElimination.reducedRowEchelonForm();
    } catch (MathArithmeticException ex) {
      // Note that we are unlikely to recover the initial matrix from the transformed matrix
      // since the exception takes place when updating a row.
      System.out.println("Transformed Matrix = \n" + gaussianElimination.getMatrix());
      throw ex;
    }
  }
}
