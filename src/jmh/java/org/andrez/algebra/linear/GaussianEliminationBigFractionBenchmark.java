package org.andrez.algebra.linear;

import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.fraction.BigFraction;
import org.openjdk.jmh.annotations.*;

public class GaussianEliminationBigFractionBenchmark {

  @State(Scope.Benchmark)
  public static class MyState extends RandomFractionVectorFactory<BigFraction> {

    @Param({"4", "8", "14", "16"})
    private int size;

    @Setup(Level.Trial)
    public void setup() {
      randomVector = createRandomVector(size * size);
    }

    @Override
    BigFraction createElement(int numerator, int denominator) {
      return new BigFraction(numerator, denominator);
    }

    @Override
    BigFraction[] createVector(int size) {
      return new BigFraction[size];
    }

    @Override
    int getSize() {
      return size;
    }
  }

  @Benchmark
  @BenchmarkMode(Mode.Throughput)
  public void reducedRowEchelonForm(GaussianEliminationBigFractionBenchmark.MyState state) {
    var gaussianElimination = state.createGaussianElimination();
    try {
      gaussianElimination.reducedRowEchelonForm();
    } catch (MathArithmeticException ex) {
      // Note that we are unlikely to recover the initial matrix from the transformed matrix
      // since the exception takes place when updating a row.
      System.out.println("Transformed Matrix = \n" + gaussianElimination.getMatrix());
      throw ex;
    }
  }
}
