package org.andrez.algebra.linear;

import java.util.Random;
import org.apache.commons.math3.FieldElement;

public abstract class RandomFractionVectorFactory<T extends FieldElement<T>> {

  private static final long SEED = 42L;
  private static final int MAX_VALUE = 4;

  protected T[] randomVector;

  protected T[] createRandomVector(int size) {
    Random rand = new Random(SEED);
    var fractions = createVector(size);
    // Use a fix denominator because of overflows when the size of the matrix increases.
    var denominator = 2;
    for (int i = 0; i < size; i++) {
      fractions[i] = createElement(rand.nextInt(MAX_VALUE), denominator);
    }
    return fractions;
  }

  public GaussianElimination<T> createGaussianElimination() {
    return new GaussianElimination<>(Matrix.create(randomVector.clone(), getSize(), getSize()));
  }

  abstract T createElement(int numerator, int denominator);

  abstract T[] createVector(int size);

  abstract int getSize();
}
