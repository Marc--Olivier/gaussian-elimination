package org.andrez.algebra.linear;

import org.apache.commons.math3.fraction.Fraction;

public interface FractionUtils {

  static Fraction r(int num, int den) {
    return new Fraction(num, den);
  }

  static Fraction r(int num) {
    return new Fraction(num);
  }
}
