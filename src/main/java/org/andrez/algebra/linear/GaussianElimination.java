package org.andrez.algebra.linear;

import org.apache.commons.math3.Field;
import org.apache.commons.math3.FieldElement;

/**
 * Contains algorithms based on Gaussian elimination.
 *
 * @param <T> Type that must satisfy the {@link org.apache.commons.math3.FieldElement FieldElement}
 *     interface.
 */
public class GaussianElimination<T extends FieldElement<T>> {

  /**
   * Creates a GaussianElimination object that operates on the given matrix.
   *
   * <p>Note that the matrix is not copied, and should not be manipulated anymore.
   *
   * @param matrix matrix manipulated by the object.
   */
  public GaussianElimination(Matrix<T> matrix) {
    if (matrix.rowCount() == 0) {
      throw new IllegalArgumentException(
          "Cannot perform a Gaussian elimination on an empty matrix");
    }
    this.matrix = matrix;
    this.field = matrix.get(0, 0).getField();
  }

  /**
   * Provides the manipulated matrix.
   *
   * @return the manipulated matrix
   */
  public Matrix<T> getMatrix() {
    return this.matrix;
  }

  /**
   * Performs the <a href="https://www.csun.edu/~panferov/math262/262_rref.pdf">Gauss-Jordan</a>
   * algorithm and returns the matrix in <a
   * href="https://en.wikipedia.org/wiki/Row_echelon_form#Reduced_row_echelon_form">reduced row
   * echelon form</a>.
   *
   * @return the matrix in <a
   *     href="https://en.wikipedia.org/wiki/Row_echelon_form#Reduced_row_echelon_form">reduced row
   *     echelon form</a>
   */
  public Matrix<T> reducedRowEchelonForm() {
    int currentRow = 0;
    int columnCount = matrix.columnCount();
    int rowCount = matrix.rowCount();
    for (int currentColumn = 0; currentColumn < columnCount; currentColumn++) {
      if (currentRow >= rowCount) {
        break;
      }
      int pivotRow = searchPivotRow(currentRow, currentColumn);
      var pivotValue = matrix.get(pivotRow, currentColumn);
      if (pivotValue.equals(zero())) {
        continue;
      }
      var pivotInverse = pivotValue.reciprocal();
      matrix.multiplyRow(pivotRow, pivotInverse);
      updateRowsUsingPivot(pivotRow, currentColumn);
      matrix.swapRows(currentRow, pivotRow);
      currentRow++;
    }
    return matrix;
  }

  private FieldElement<T> zero() {
    return field.getZero();
  }

  private int searchPivotRow(int fromRow, int column) {
    for (int i = fromRow; i < matrix.rowCount(); i++) {
      if (!matrix.get(i, column).equals(zero())) {
        return i;
      }
    }
    return fromRow;
  }

  private void updateRowsUsingPivot(int pivotRow, int pivotColumn) {
    for (int i = 0; i < matrix.rowCount(); ++i) {
      if (i == pivotRow) {
        continue;
      }
      matrix.substractMultipliedRow(i, pivotRow, matrix.get(i, pivotColumn));
    }
  }

  private final Matrix<T> matrix;
  private final Field<T> field;
}
