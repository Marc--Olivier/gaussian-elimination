package org.andrez.algebra.linear;

import org.andrez.algebra.linear.internal.FieldMatrix;
import org.apache.commons.math3.FieldElement;

/**
 * Representation of a mutable matrix.
 *
 * @param <T> type of the values contained in the Matrix.
 */
public interface Matrix<T> {

  /**
   * Creates a matrix of numbers that satisfy the FieldElement trait.
   *
   * <p>Note that the values are copied.
   *
   * @param values values copied to the matrix such that each row contains the same number of
   *     columns
   * @param <T> type of the input values
   * @return the matrix of numbers
   */
  static <T extends FieldElement<T>> Matrix<T> create(T[][] values) {
    return FieldMatrix.from(values);
  }

  /**
   * Creates a matrix of numbers that satisfy the FieldElement trait.
   *
   * <p>Note that the values are not copied, and must not be modified after the matrix creation.
   *
   * @param values values contained in the matrix such that {@code values.length == rowCount *
   *     columnCount}
   * @param rowCount number of rows
   * @param columnCount number of columns
   * @param <T> type of the input values
   * @return the matrix of numbers
   */
  static <T extends FieldElement<T>> Matrix<T> create(T[] values, int rowCount, int columnCount) {
    return new FieldMatrix<T>(values, rowCount, columnCount);
  }

  /**
   * Returns the number of columns, 0 if the matrix is empty.
   *
   * @return the number of columns
   */
  int rowCount();

  /**
   * Returns the number of rows, 0 if the matrix is empty.
   *
   * @return the number of rows
   */
  int columnCount();

  /**
   * Return the object at the given position (i,j)
   *
   * @param i index of the row
   * @param j index of the column
   * @return object at the position (i,j)
   * @throws ArrayIndexOutOfBoundsException if {@code i >= rowCount() || j >= columnCount()}
   */
  T get(int i, int j);

  /**
   * Return the values contained in the matrix.
   *
   * <p>Note that the returned values are not copied, and must not be modified.
   *
   * @return the values contained in the matrix.
   */
  T[] values();

  /**
   * Swap two rows of the matrix and returns the updated matrix.
   *
   * @param row1 first row
   * @param row2 second row
   * @return updated matrix with the swapped rows.
   * @throws ArrayIndexOutOfBoundsException if {@code row1 >= rowCount() || row2 >= rowCount()}
   */
  Matrix<T> swapRows(int row1, int row2);

  /**
   * In-place multiplication of the values of the given row by the given value.
   *
   * @param rowIndex index of the row which values get multiplied
   * @param value value of the multiplicand.
   * @return the updated matrix.
   * @throws ArrayIndexOutOfBoundsException if {@code rowIndex >= rowCount()}
   */
  Matrix<T> multiplyRow(int rowIndex, T value);

  /**
   * Performs {@code matrix[targetRow] <- matrix[targetRow] - factor * matrix[srcRow]} and returns
   * the updated matrix.
   *
   * @param targetRow index of the updated row
   * @param otherRow index of the row that is multiplied by factor
   * @param factor factor used in the multiplication
   * @return the updated matrix
   * @throws ArrayIndexOutOfBoundsException if {@code targetRow >= rowCount() || otherRow >=
   *     rowCount()}
   */
  Matrix<T> substractMultipliedRow(int targetRow, int otherRow, T factor);
}
