package org.andrez.algebra.linear.internal;

import java.lang.reflect.Array;
import org.andrez.algebra.linear.Matrix;
import org.apache.commons.math3.Field;
import org.apache.commons.math3.FieldElement;

public class FieldMatrix<T extends FieldElement<T>> implements Matrix<T> {

  public FieldMatrix(T[] values, int rowCount, int columnCount) {
    if (values.length == 0) {
      throw new IllegalArgumentException("Cannot create an empty matrix");
    }
    int expectedValuesCount = rowCount * columnCount;
    if (values.length != expectedValuesCount) {
      throw new IllegalArgumentException(
          String.format("Expected %d values, got %d", expectedValuesCount, values.length));
    }
    this.values = values;
    this.rowCount = rowCount;
    this.columnCount = columnCount;
    this.field = values[0].getField();
  }

  @SuppressWarnings("unchecked")
  public static <T extends FieldElement<T>> FieldMatrix<T> from(T[][] values) {
    int rowCount = values.length;
    int columnCount = columnCount(values);
    int elementCount = rowCount * columnCount;
    if (elementCount == 0) {
      throw new IllegalArgumentException("Cannot create an empty matrix");
    }
    T[] linearValues = (T[]) Array.newInstance(values[0][0].getClass(), elementCount);
    for (int i = 0; i < rowCount; ++i) {
      System.arraycopy(values[i], 0, linearValues, i * columnCount, columnCount);
    }
    return new FieldMatrix<>(linearValues, rowCount, columnCount);
  }

  private static <T> int columnCount(T[][] values) {
    if (values.length == 0) {
      return 0;
    }
    var columnCount = values[0].length;
    for (int i = 1; i < values.length; i++) {
      var row = values[i];
      if (row.length != columnCount) {
        throw new IllegalArgumentException(
            String.format("At row %d, expected %d columns, got %d.", i, columnCount, row.length));
      }
    }
    return columnCount;
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    addRowToString(builder, 0);
    for (int i = 1; i < rowCount; i++) {
      builder.append("\n");
      addRowToString(builder, i);
    }
    return builder.toString();
  }

  private void addRowToString(StringBuilder builder, int rowIndex) {
    builder.append(get(rowIndex, 0));
    for (int j = 1; j < columnCount; j++) {
      builder.append(", ");
      builder.append(get(rowIndex, j));
    }
    builder.append(";");
  }

  @Override
  public int rowCount() {
    return this.rowCount;
  }

  @Override
  public int columnCount() {
    return this.columnCount;
  }

  @Override
  public T get(int i, int j) {
    if (j >= columnCount) {
      throw new ArrayIndexOutOfBoundsException(
          String.format("Matrix column index out of range: %d >= %d", j, columnCount));
    } else if (i >= rowCount) {
      throw new ArrayIndexOutOfBoundsException(
          String.format("Matrix row index out of range: %d >= %d", i, rowCount));
    } else {
      return values[i * columnCount + j];
    }
  }

  @Override
  public T[] values() {
    return this.values;
  }

  @Override
  public FieldMatrix<T> multiplyRow(int rowIndex, T value) {
    int index = rowIndex * columnCount;
    for (int j = 0; j < columnCount; j++) {
      values[index] = values[index].multiply(value);
      index++;
    }
    return this;
  }

  @Override
  public FieldMatrix<T> substractMultipliedRow(int targetRow, int otherRow, T factor) {
    if (factor.equals(field.getZero())) {
      return this;
    }
    var factorOpposite = factor.negate();
    int targetIndex = targetRow * columnCount;
    int otherIndex = otherRow * columnCount;
    for (int j = 0; j < columnCount; j++) {
      values[targetIndex] = values[targetIndex].add(values[otherIndex].multiply(factorOpposite));
      targetIndex++;
      otherIndex++;
    }
    return this;
  }

  @Override
  public FieldMatrix<T> swapRows(int row1, int row2) {
    int index1 = row1 * columnCount;
    int index2 = row2 * columnCount;
    for (int j = 0; j < columnCount; j++) {
      var tmp = values[index1];
      values[index1] = values[index2];
      values[index2] = tmp;
      index1++;
      index2++;
    }
    return this;
  }

  private final T[] values;
  private final int rowCount;
  private final int columnCount;
  private final Field<T> field;
}
