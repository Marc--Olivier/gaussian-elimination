package org.andrez.algebra.linear;

import static org.andrez.algebra.linear.FractionUtils.r;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FractionUtilsTest {

  @Test
  void createFraction() {
    var fraction = r(3, 4);
    assertEquals(3, fraction.getNumerator());
    assertEquals(4, fraction.getDenominator());
    assertEquals(0.75, fraction.doubleValue());
  }

  @Test
  void createFractionFromInt() {
    var fraction = r(3);
    assertEquals(3, fraction.getNumerator());
    assertEquals(1, fraction.getDenominator());
  }

  @Test
  void simplifyFractionOnCreation() {
    var fraction = r(6, 8);
    assertEquals(3, fraction.getNumerator());
    assertEquals(4, fraction.getDenominator());
  }

  @Test
  void exceptionWhenDividingBy0() {
    assertThrows(ArithmeticException.class, () -> r(1, 0));
  }

  @Test
  void fractionsEquality() {
    var f1 = r(6, 8);
    assertEquals(r(3, 4), f1);
    assertNotEquals(r(3, 5), f1);
  }

  @Test
  void multiplyFractions() {
    var f1 = r(2, 9);
    var f2 = r(3, 4);
    assertEquals(r(1, 6), f1.multiply(f2));
  }

  @Test
  void overflowWhenMultiplyingFractions() {
    var max = Integer.MAX_VALUE;
    var f1 = r(max, max - 1);
    var f2 = r(max - 2, max - 3);
    assertThrows(ArithmeticException.class, () -> f1.multiply(f2));
  }

  @Test
  void addTwoFractions() {
    var f1 = r(1, 2);
    var f2 = r(1, 3);
    assertEquals(r(5, 6), f1.add(f2));
  }

  @Test
  void overflowWhenAddingFractions() {
    var max = Integer.MAX_VALUE;
    var f1 = r(1, max);
    var f2 = r(1, max - 1);
    assertThrows(ArithmeticException.class, () -> f1.add(f2));
  }
}
