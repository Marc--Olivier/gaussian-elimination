package org.andrez.algebra.linear;

import static org.andrez.algebra.linear.FractionUtils.r;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import org.apache.commons.math3.fraction.Fraction;
import org.junit.jupiter.api.Test;

class GaussianEliminationTest {

  @Test
  void getMatrix() {
    Fraction[][] data = {
      {r(2), r(-1), r(0)},
      {r(-1), r(2), r(-1)},
      {r(0), r(-1), r(2)}
    };
    var matrix = Matrix.create(data);
    var gaussianElimination = new GaussianElimination<>(matrix);

    assertEquals(matrix, gaussianElimination.getMatrix());
  }

  @Test
  void reducedEchelonFormOfSquareMatrix() {
    // https://en.wikipedia.org/wiki/Gaussian_elimination#Finding_the_inverse_of_a_matrix
    Fraction[][] data = {
      {r(2), r(-1), r(0)},
      {r(-1), r(2), r(-1)},
      {r(0), r(-1), r(2)}
    };
    var gaussianElimination = new GaussianElimination<>(Matrix.create(data));

    var expected = Arrays.asList(r(1), r(0), r(0), r(0), r(1), r(0), r(0), r(0), r(1));

    assertEquals(expected, Arrays.asList(gaussianElimination.reducedRowEchelonForm().values()));
  }

  @Test
  void reducedEchelonFormOfSquareMatrixNotFullRank() {
    // Example from https://en.wikipedia.org/wiki/Gaussian_elimination
    Fraction[][] data = {
      {r(1), r(3), r(1)},
      {r(1), r(1), r(-1)},
      {r(3), r(11), r(5)}
    };

    var gaussianElimination = new GaussianElimination<>(Matrix.create(data));

    var expected = Arrays.asList(r(1), r(0), r(-2), r(0), r(1), r(1), r(0), r(0), r(0));

    assertEquals(expected, Arrays.asList(gaussianElimination.reducedRowEchelonForm().values()));
  }

  @Test
  void reducedEchelonFormOfTriangularMatrix() {
    Fraction[][] data = {
      {r(1), r(0), r(0), r(1)},
      {r(0), r(1), r(0), r(1)},
      {r(0), r(0), r(1), r(1)},
    };
    var gaussianElimination = new GaussianElimination<>(Matrix.create(data));

    var expected =
        Arrays.asList(
            r(1), r(0), r(0), r(1), //
            r(0), r(1), r(0), r(1), //
            r(0), r(0), r(1), r(1));

    var reducedRowEchelonForm = gaussianElimination.reducedRowEchelonForm();
    assertEquals(expected, Arrays.asList(reducedRowEchelonForm.values()));
  }

  @Test
  void reducedEchelonFormOfTriangularMatrixNotFulRank() {
    // Example from https://people.revoledu.com/kardi/tutorial/LinearAlgebra/RREF.html
    Fraction[][] data = {
      {r(3), r(2), r(2), r(3), r(1)},
      {r(6), r(4), r(4), r(6), r(2)},
      {r(9), r(6), r(6), r(9), r(1)},
    };
    var gaussianElimination = new GaussianElimination<>(Matrix.create(data));

    var expected =
        Arrays.asList(
            r(1), r(2, 3), r(2, 3), r(1), r(0), //
            r(0), r(0), r(0), r(0), r(1), //
            r(0), r(0), r(0), r(0), r(0));

    var reducedRowEchelonForm = gaussianElimination.reducedRowEchelonForm();
    assertEquals(expected, Arrays.asList(reducedRowEchelonForm.values()));
  }
}
