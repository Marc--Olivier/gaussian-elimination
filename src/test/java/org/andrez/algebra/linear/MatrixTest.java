package org.andrez.algebra.linear;

import static org.andrez.algebra.linear.FractionUtils.*;
import static org.junit.jupiter.api.Assertions.*;

import org.apache.commons.math3.fraction.Fraction;
import org.junit.jupiter.api.Test;

class MatrixTest {

  @Test
  void createRationalMatrix() {
    Fraction[][] data = {
      {r(1, 1), r(1, 2), r(1, 3)},
      {r(2, 1), r(2, 2), r(2, 3)}
    };
    var m = Matrix.create(data);
    assertEquals(r(2, 3), m.get(1, 2));
  }

  @Test
  void createRationalMatrixFrom1DArray() {
    Fraction[] data = {
      r(1, 1), r(1, 2), r(1, 3),
      r(2, 1), r(2, 2), r(2, 3)
    };
    var m = Matrix.create(data, 2, 3);
    assertEquals(r(2, 3), m.get(1, 2));
  }
}
