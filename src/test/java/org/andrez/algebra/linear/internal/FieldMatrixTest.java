package org.andrez.algebra.linear.internal;

import static org.andrez.algebra.linear.FractionUtils.r;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import org.apache.commons.math3.fraction.Fraction;
import org.junit.jupiter.api.Test;

class FieldMatrixTest {

  @Test
  void newMatrixFrom1DArray() {
    Fraction[] data = {
      r(2), r(-1), r(0),
      r(-1), r(2), r(-1)
    };
    var matrix = new FieldMatrix<>(data, 2, 3);

    assertEquals(Arrays.asList(data), Arrays.asList(matrix.values()));
    assertEquals(2, matrix.rowCount());
    assertEquals(3, matrix.columnCount());
  }

  @Test
  void newEmptyMatrixFrom1DArray() {
    Fraction[] data = {};
    assertThrows(IllegalArgumentException.class, () -> new FieldMatrix<>(data, 0, 0));
  }

  @Test
  void newMatrixWithInvalidValuesCount() {
    Fraction[] data = {
      r(2), r(-1), r(0),
      r(-1), r(2), r(-1)
    };
    assertThrows(IllegalArgumentException.class, () -> new FieldMatrix<>(data, 2, 4));
  }

  @Test
  void newMatrix() {
    Fraction[][] data = {
      {r(2), r(-1), r(0)},
      {r(-1), r(2), r(-1)}
    };
    var matrix = FieldMatrix.from(data);

    var expected = Arrays.asList(r(2), r(-1), r(0), r(-1), r(2), r(-1));
    assertEquals(expected, Arrays.asList(matrix.values()));
    assertEquals(2, matrix.rowCount());
    assertEquals(3, matrix.columnCount());
  }

  @Test
  void newEmptyMatrix() {
    assertThrows(IllegalArgumentException.class, () -> FieldMatrix.from(new Fraction[][] {{}}));
    assertThrows(IllegalArgumentException.class, () -> FieldMatrix.from(new Fraction[][] {}));
  }

  @Test
  void newMatrixWithInvalidSize() {
    Fraction[][] data = {
      {r(1, 1), r(1, 2)},
      {r(2, 1), r(2, 2), r(2, 3)}
    };
    assertThrows(IllegalArgumentException.class, () -> FieldMatrix.from(data));
  }

  @Test
  void matrixToString() {
    Fraction[][] data = {
      {r(1, 1), r(1, 2), r(1, 3)},
      {r(2, 1), r(2, 2), r(2, 3)}
    };

    String expected = "1, 1 / 2, 1 / 3;\n2, 1, 2 / 3;";

    assertEquals(expected, FieldMatrix.from(data).toString());
  }

  @Test
  void get() {
    Fraction[][] data = {
      {r(1), r(2), r(3), r(4)},
      {r(5), r(6), r(7), r(8)},
      {r(9), r(10), r(11), r(12)}
    };
    var matrix = FieldMatrix.from(data);

    assertEquals(r(10), matrix.get(2, 1));
  }

  @Test
  void getOutOfBounds() {
    Fraction[] data = {r(1)};
    var matrix = new FieldMatrix<>(data, 1, 1);

    assertThrows(ArrayIndexOutOfBoundsException.class, () -> matrix.get(1, 0));
    assertThrows(ArrayIndexOutOfBoundsException.class, () -> matrix.get(0, 1));
  }

  @Test
  void multiplyRow() {
    Fraction[][] data = {
      {r(1), r(2), r(3), r(4)},
      {r(5), r(6), r(7), r(8)},
      {r(9), r(10), r(11), r(12)}
    };
    var matrix = FieldMatrix.from(data);

    var expected =
        Arrays.asList(
            r(1), r(2), r(3), r(4), r(5), r(6), r(7), r(8), r(27, 4), r(15, 2), r(33, 4), r(9));
    assertEquals(expected, Arrays.asList(matrix.multiplyRow(2, r(3, 4)).values()));
  }

  @Test
  void multiplyRowOutOfBounds() {
    Fraction[] data = {r(1)};
    var matrix = new FieldMatrix<>(data, 1, 1);

    assertThrows(ArrayIndexOutOfBoundsException.class, () -> matrix.multiplyRow(1, r(1)));
  }

  @Test
  void substractMultipliedRow() {
    Fraction[][] data = {
      {r(1), r(2), r(3), r(4)},
      {r(5), r(6), r(7), r(8)},
      {r(9), r(10), r(11), r(12)}
    };
    var matrix = FieldMatrix.from(data);

    var expected =
        Arrays.asList(
            r(1), r(2), r(3), r(4), r(-22), r(-24), r(-26), r(-28), r(9), r(10), r(11), r(12));
    assertEquals(expected, Arrays.asList(matrix.substractMultipliedRow(1, 2, r(3)).values()));
  }

  @Test
  void substractMultipliedRowOutOfBounds() {
    Fraction[] data = {r(1)};
    var matrix = new FieldMatrix<>(data, 1, 1);

    assertThrows(
        ArrayIndexOutOfBoundsException.class, () -> matrix.substractMultipliedRow(0, 1, r(1)));
    assertThrows(
        ArrayIndexOutOfBoundsException.class, () -> matrix.substractMultipliedRow(1, 0, r(1)));
  }

  @Test
  void swapRows() {
    Fraction[][] data = {
      {r(1), r(2), r(3), r(4)},
      {r(5), r(6), r(7), r(8)},
      {r(9), r(10), r(11), r(12)}
    };
    var matrix = FieldMatrix.from(data);

    var expected =
        Arrays.asList(r(1), r(2), r(3), r(4), r(9), r(10), r(11), r(12), r(5), r(6), r(7), r(8));
    assertEquals(expected, Arrays.asList(matrix.swapRows(1, 2).values()));
  }

  @Test
  void swapRowsOutOfBounds() {
    Fraction[] data = {r(1)};
    var matrix = new FieldMatrix<>(data, 1, 1);

    assertThrows(ArrayIndexOutOfBoundsException.class, () -> matrix.swapRows(0, 1));
    assertThrows(ArrayIndexOutOfBoundsException.class, () -> matrix.swapRows(1, 0));
  }
}
